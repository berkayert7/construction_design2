using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UserController : MonoBehaviour
{
    public static int userType = 0;
    public void User1()         //Kullanıcı tipi 1 ise ana sahneye yöndendirilecek.
    {
        userType = 1;
        SceneManager.LoadScene("MainScene");
    }
    public void User2()         //Kullanıcı tipi 2 ise ana sahneye yöndendirilecek.
    {
        userType = 2;
        SceneManager.LoadScene("MainScene");
    }
    public void User3()         //Kullanıcı tipi 3 ise ana sahneye yöndendirilecek.
    {
        userType = 3;
        SceneManager.LoadScene("MainScene");
    }

    public void Exitgame()
    {
        Application.Quit();
    }
}
