using UnityEngine;

public class MaterialChangeOnTrigger : MonoBehaviour
{
    public Material mat1; // Birinci Materyal
    public Material matGreen; // İkinci Materyal
    public Material matPurple;
    public Material matRed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MatGreen")) // Tetikleyici etiketi "Player" olan bir GameObject ise,
        {
            Renderer renderer = GetComponent<Renderer>(); // Objemizin Renderer bileşenini alalım

            if (renderer != null) // Renderer bileşeni varsa,
            {
                renderer.material = matGreen; // Materyal değiştirme işlemi
            }
        }
        if (other.CompareTag("MatPurple")) // Tetikleyici etiketi "Player" olan bir GameObject ise,
        {
            Renderer renderer = GetComponent<Renderer>(); // Objemizin Renderer bileşenini alalım

            if (renderer != null) // Renderer bileşeni varsa,
            {
                renderer.material = matPurple; // Materyal değiştirme işlemi
            }
        }
        if (other.CompareTag("MatRed")) // Tetikleyici etiketi "Player" olan bir GameObject ise,
        {
            Renderer renderer = GetComponent<Renderer>(); // Objemizin Renderer bileşenini alalım

            if (renderer != null) // Renderer bileşeni varsa,
            {
                renderer.material = matRed; // Materyal değiştirme işlemi
            }
        }
    }
}
