using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallPainter : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Firca")) // Tetikleyici etiketi "Firca" olan bir GameObject ise,
        {
            Renderer renderer = other.GetComponent<Renderer>(); // Tetikleyici objenin Renderer bileşenini alalım

            if (renderer != null) // Renderer bileşeni varsa,
            { 
                this.GetComponent<Renderer>().material = renderer.material; // Materyal değiştirme işlemi
            }
        }
    }
}
