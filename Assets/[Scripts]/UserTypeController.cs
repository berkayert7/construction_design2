using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserTypeController : MonoBehaviour
{
    public GameObject Chalk1;
    public GameObject Chalk2;
    public GameObject Chalk3;
    private int userType;
    
    // Start is called before the first frame update
    void Start()
    {
        try
        {
            userType = UserController.userType;
            switch (userType)
            {
                case 1:
                    Chalk1.gameObject.SetActive(true);
                    break;
                case 2:
                    Chalk2.gameObject.SetActive(true);
                    break;
                case 3:
                    Chalk3.gameObject.SetActive(true);
                    break;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
       
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
