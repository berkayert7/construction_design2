using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonModel : MonoBehaviour
{
    public float movementSpeed = 5f;           // karakterin hareket hızı
    public float jumpForce = 7f;               // karakterin zıplama kuvveti
    public float gravityScale = 2.5f;          // karakterin yerçekimi ivmesi
    public float mouseSensitivity = 100f;      // fare hassasiyeti

    public float verticalRotation = 0f;        // dikey dönüş açısı
    public float horizontalRotation = 0f;      // yatay dönüş açısı
    public float upDownRange = 60f;            // dikey dönüş açı aralığı

    public Vector3 velocity;                   // karakterin hızı
}