using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonView : MonoBehaviour
{
    public Transform groundCheck;              // karakterin yer temasını kontrol etmek için kullanılacak nesne
    public FirstPersonController2 controller;  // karakterin kontrolcüsü

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;    // fare imlecini kilitle
    }

    void Update()
    {
        // Fare hareketi
        float rotLeftRight = Input.GetAxis("Mouse X") * controller.model.mouseSensitivity * Time.deltaTime; // fare yatay hareketi
        float rotUpDown = -Input.GetAxis("Mouse Y") * controller.model.mouseSensitivity * Time.deltaTime;  // fare dikey hareketi

        controller.model.verticalRotation += rotUpDown;                         // dikey dönüş açısını güncelle
        controller.model.verticalRotation = Mathf.Clamp(controller.model.verticalRotation, -controller.model.upDownRange, controller.model.upDownRange);

        controller.model.horizontalRotation += rotLeftRight;                     // yatay dönüş açısını güncelle
        controller.transform.rotation = Quaternion.Euler(0, controller.model.horizontalRotation, 0);  // karakterin yatay dönüş açısını güncelle
        Camera.main.transform.localRotation = Quaternion.Euler(controller.model.verticalRotation, 0, 0);  // karakterin dikey dönüş açısını güncelle
        
        // Zıplama
        if (Input.GetKeyDown(KeyCode.Space))                            //Eğer Space tuşuna basıldıysa,
        {
            controller.model.velocity.y = controller.model.jumpForce;           //Kontrolcünün modelindeki y dikey hız değeri, kontrolcünün modelindeki zıplama kuvveti değeri olacak şekilde atanır. Bu, karakterin zıplamasını sağlar.
        }
    }
}