using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// FirstPersonModel ve FirstPersonView script'lerine erişim sağlamak için değişkenler tanımlanıyor.
public class FirstPersonController2 : MonoBehaviour
{
    public FirstPersonModel model; // karakter modeli
    public FirstPersonView view; // kamera
    private CharacterController characterController;

    void Start()
    {
        // Karakter kontrolcüsü alınıyor.
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        // Hareket
        float forwardSpeed = Input.GetAxis("Vertical") * model.movementSpeed; // İleri-geri hareket hızı
        float sideSpeed = Input.GetAxis("Horizontal") * model.movementSpeed; // Yan hareket hızı

        Vector3 speed = new Vector3(sideSpeed, 0, forwardSpeed);
        speed = transform.rotation * speed; // Hareketin karakterin bakış yönüne göre dönmesi sağlanıyor.
        
        
        // Yerçekimi uygulanıyor.
        model.velocity.y += Physics.gravity.y * model.gravityScale * Time.deltaTime;

        // Karakter hareket ettiriliyor.
        characterController.Move((model.velocity + speed) * Time.deltaTime);
    }
}